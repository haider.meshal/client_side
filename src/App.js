import React from 'react';
import { Store } from 'state/Store';
import { BrowserRouter, Redirect, Switch } from 'react-router-dom';
import componentQueries from 'react-component-queries';

import LayoutRoute from 'components/LayoutRoute';
import EmptyLayout from 'components/EmptyLayout';
import MainLayout from 'components/MainLayout';

import Login from 'pages/Login';
import Dashboard from 'pages/Dashboard';
import Records from 'pages/Records';
import Attendances from 'pages/Attendances';
import Finance from 'pages/Finance';
import Archive from 'pages/Archive';
import Settings from 'pages/Settings';
import Logout from 'pages/Logout';
import 'styles/app.scss';

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

const App = () => {
  const { state } = React.useContext(Store);
  if (state.app.loading) return <div>LOADING</div>;
  if (state.app.success) return <div>SUCCESS</div>;
  if (state.app.failed) return <div>FAILED</div>;
  return (
    <BrowserRouter basename={getBasename()}>
      <Switch>
        <LayoutRoute
          exact
          layout={EmptyLayout}
          path="/login"
          component={Login}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/dashboard"
          component={Dashboard}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/records"
          component={Records}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/attendances"
          component={Attendances}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/finance"
          component={Finance}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/archive"
          component={Archive}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/settings"
          component={Settings}
        />
        <LayoutRoute
          exact
          layout={MainLayout}
          path="/logout"
          component={Logout}
        />
        <Redirect to="/login" />
      </Switch>
    </BrowserRouter>
  );
};

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
