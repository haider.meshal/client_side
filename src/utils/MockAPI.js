const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

export const GetHeirarchies = async () => {
  const result = {
    '1': {
      name: 'Divisional Structure',
    },
    '2': {
      name: 'Team-based Organizational Structure',
    },
    '3': {
      name: 'Functional Organizatinal Structure',
    },
    '4': {
      name: 'Matrix Organizational Structure',
    },
  };
  await sleep(5000);
  return result;
};

export const ListHeirarchies = async () => {
  const result = [
    {
      name: 'Divisional Structure',
    },
    {
      name: 'Team-based Organizational Structure',
    },
    {
      name: 'Functional Organizatinal Structure',
    },
    {
      name: 'Matrix Organizational Structure',
    },
  ];
  await sleep(5000);
  return result;
};
