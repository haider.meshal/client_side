import axios from "axios";

const facade = {};

const api = axios.create({ baseURL: "/api" });

facade.request = config => api.request(config);

["get", "head"].forEach(async method => {
  facade[method] = async (url, config) =>
    await facade.request({ ...config, method, url });
});

["delete", "post", "put", "patch"].forEach(method => {
  facade[method] = async (url, data, config) =>
    await facade.request({ ...config, method, url, data });
});

export const setToken = async token => {
  await localStorage.setItem("token", token);
};

export const getToken = async () => {
  await localStorage.getItem("token");
};

export const removeToken = async () => {
  await localStorage.removeItem("token");
};

export const registerAPI = async (email, password, name, phone) => {
  const httpRequest = await facade.post("/subscribers/register", {
    email: email,
    password: password,
    name: name,
    phone: phone
  });
  const result = Object.assign({status: httpRequest.statusText == "OK"},httpRequest.data);
  return result;
};

export const loginAPI = async (email, password) => {
  const httpRequest = await facade.post("/subscribers/signin", {
    email: email,
    password: password
  });
  const result = Object.assign({status: httpRequest.statusText == "OK"},httpRequest.data);
  return result;
};

export const forgetpasswordAPI = async email => {
  return await facade.post("/subscribers/forgetpassword", { email: email });
};

export const activateaccountAPI = async code => {
  const config = {
    headers: { Authorization: "Bearer " + getToken() }
  };
  return await facade.post(
    "/subscribers/activateaccount",
    { code: code },
    config
  );
};

export const logoutAPI = async () => {
  const config = {
    headers: { Authorization: "Bearer " + getToken() }
  };
  removeToken();
  return await facade.post("/subscribers/logout", {}, config);
};

export const listPostsAPI = async () => {
  const httpRequest = await facade.post("/data/posts", {});
  const result = Object.assign({status: httpRequest.statusText == "OK"},httpRequest.data);
  return result;
};

export const listClassificationsAPI = async () => {
  const httpRequest = await facade.get("/data/classifications", {});
  const result = Object.assign({status: httpRequest.statusText == "OK"},httpRequest.data);
  return result;
}
