import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const Archive = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="ARCHIVE"
      breadcrumbs={[{ name: 'ARCHIVE', active: true }]}
    >
      <Row>
        <Col>
          <Alert color="primary">
            <pre>ARCHIVE PAGE</pre>
          </Alert>
        </Col>
      </Row>
    </Page>
  );
};
export default Archive;
