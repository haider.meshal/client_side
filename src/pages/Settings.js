import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const Settings = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="SETTINGS"
      breadcrumbs={[{ name: 'SETTINGS', active: true }]}
    >
      <Row>
        <Col>
          <Alert color="primary">
            <pre>SETTINGS PAGE</pre>
          </Alert>
        </Col>
      </Row>
    </Page>
  );
};
export default Settings;
