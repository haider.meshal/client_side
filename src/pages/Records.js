import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const ImportScreen = dispatch => {
  return <pre>Import Screen</pre>;
};

const AddRecordScreen = dispatch => {
  return <pre>Add Screen</pre>;
};

const EditRecordScreen = dispatch => {
  return <pre>Edit Screen</pre>;
};

const DeleteRecordScreen = dispatch => {
  return <pre>Delete Screen</pre>;
};

const ListScreen = dispatch => {
  return (
    <Row>
          <Col xs={12} md={2}><Alert color="primary">FILTER </Alert></Col>
          <Col xs={12} md={10}><Alert color="primary">RECORD LIST </Alert></Col>
    </Row>
  );
};

const Records = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="RECORDS"
      breadcrumbs={[{ name: 'RECORDS', active: true }]}
    >
      <Row>
        <Col>
            {state.app.screen === 0 ? ListScreen(dispatch) : <div />}
            {state.app.screen === 1 ? ImportScreen(dispatch) : <div />}
            {state.app.screen === 2 ? AddRecordScreen(dispatch) : <div />}
            {state.app.screen === 3 ? EditRecordScreen(dispatch) : <div />}
            {state.app.screen === 4 ? DeleteRecordScreen(dispatch) : <div />}
        </Col>
      </Row>
    </Page>
  );
};
export default Records;
