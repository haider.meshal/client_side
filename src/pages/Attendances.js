import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const Attendances = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="ATTENDANCES"
      breadcrumbs={[{ name: 'ATTENDANCES', active: true }]}
    >
      <Row>
        <Col>
          <Alert color="primary">
            <pre>ATTENDANCES PAGE</pre>
          </Alert>
        </Col>
      </Row>
    </Page>
  );
};
export default Attendances;
