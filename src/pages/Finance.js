import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const Finance = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="FINANCE"
      breadcrumbs={[{ name: 'FINANCE', active: true }]}
    >
      <Row>
        <Col>
          <Alert color="primary">
            <pre>FINANCE PAGE</pre>
          </Alert>
        </Col>
      </Row>
    </Page>
  );
};
export default Finance;
