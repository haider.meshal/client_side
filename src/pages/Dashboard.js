import React from 'react';
import { Alert, Button, Row, Col } from 'reactstrap';
import Page from 'components/Page';
import { Store } from 'state/Store';

import {NORMAL, LOADING, SUCCESS, FAILED, SIGNINED_ON, SIGNINED_OFF} from 'state/reducers';
import GridEditor from 'components/GridEditor';

const dataSchema = {
  code: {
    order: 1,
    input: 'text',
    title: 'CODE',
    editable: true,
    validate: '',
  },
  title: {
    order: 2,
    input: 'text',
    title: 'TITLE',
    editable: true,
    validate: '',
  },
};

const data = {
   0: {code: 'code 1', title: 'title 1'},
   1: {code: 'code 2', title: 'title 2'},
   2: {code: 'code 3', title: 'title 3'},
   3: {code: 'code 4', title: 'title 4'}
}

const Dashboard = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="DASHBOARD"
      breadcrumbs={[{ name: 'DASHBOARD', active: true }]}
    >
      <Row>
        <Col>
            <GridEditor data={data} dataSchema={dataSchema}/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Alert color="primary">
            <pre>{JSON.stringify(state, null, 4)}</pre>
          </Alert>
        </Col>
        <Col>
          <Button onClick= {() => dispatch({type: NORMAL})}> NORMAL </Button>
          <Button onClick= {() => dispatch({type: LOADING, payload: 'WAIT'})}> LOADING </Button>
          <Button onClick= {() => dispatch({type: SUCCESS, payload: 'SUCCESS'})}> SUCCESS </Button>
          <Button onClick= {() => dispatch({type: FAILED, payload: 'ERROR'})}> ERROR </Button>
          <Button onClick= {() => dispatch({type: SIGNINED_ON, payload: {name: 'haider', token: 'xxx', profile_image:'http://url...'}})}> SIGNINED_ON </Button>
          <Button onClick= {() => dispatch({type: SIGNINED_OFF})}> SIGNINED_OFF</Button>
        </Col>
      </Row>
    </Page>
  );
};
export default Dashboard;
