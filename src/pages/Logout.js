
import React from 'react';
import Page from 'components/Page';
import { Store } from 'state/Store';

import { Alert, Button, Row, Col } from 'reactstrap';

const Logout = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Page
      className="DashboardPage"
      title="LOGOUT"
      breadcrumbs={[{ name: 'LOGOUT', active: true }]}
    >
      <Row>
        <Col>
          <Alert color="primary">
            <pre>LOGOUT PAGE</pre>
          </Alert>
        </Col>
      </Row>
    </Page>
  );
};
export default Logout;
