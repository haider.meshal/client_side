import React from 'react';
import { Store } from 'state/Store';
import {SWITCH_SCREEN} from 'state/reducers';

import { Navbar, Nav, Button } from 'reactstrap';
import { MdClearAll } from 'react-icons/md';

import SearchInput from 'components/SearchInput';
import bn from 'utils/bemnames';
const bem = bn.create('header');

const handleSidebarControlButton = event => {
  event.preventDefault();
  event.stopPropagation();
  document.querySelector('.cr-sidebar').classList.toggle('cr-sidebar--open');
};

const  DashboardScreens = (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> DASH SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> DASH SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> DASH SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> DASH SCREEN 3 </Button>

  </div>
  );
}

const  RecordsScreens= (dispatch) => {
  return (
    <div>
    <Button outline color="primary" onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> LIST </Button>{' '}
    <Button outline color="primary" onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> IMPORT </Button>{' '}
    <Button outline color="primary" onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> ADD </Button>{' '}
    <Button outline color="primary" onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> EDIT </Button>{' '}
    <Button outline color="primary" onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 4})}> DELETE </Button>{' '}
  </div>
  );
}

const  AttendancesScreens= (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> ATT SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> ATT SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> ATT SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> ATT SCREEN 3 </Button>

  </div>
  );
}

const  FinanceScreens= (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> FINANCE SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> FINANCE SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> FINANCE SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> FINANCE SCREEN 3 </Button>

  </div>
  );
}
const  ArchiveScreens= (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> ARCHIVE SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> ARCHIVE SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> ARCHIVE SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> ARCHIVE SCREEN 3 </Button>

  </div>
  );
}

const  SettingsScreens= (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> SETTINGS SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> SETTINGS SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> SETTINGS SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> SETTINGS SCREEN 3 </Button>

  </div>
  );
}

const  LogoutScreens= (dispatch) => {
  return (
    <div>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 0})}> LOGOUT SCREEN 0 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 1})}> LOGOUT SCREEN 1 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 2})}> LOGOUT SCREEN 2 </Button>
    <Button onClick= {() => dispatch({type: SWITCH_SCREEN, payload: 3})}> LOGOUT SCREEN 3 </Button>

  </div>
  );
}
const Header = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <Navbar light expand className={bem.b('bg-white')}>
      <Nav navbar className="mr-2">
        <Button outline onClick={handleSidebarControlButton}>
          <MdClearAll size={25} />
        </Button>
      </Nav>
        <Nav navbar>
          <SearchInput />
          <p>&nbsp;&nbsp;&nbsp;</p>
            {state.app.page === 1 ?  DashboardScreens(dispatch): <div />}
            {state.app.page === 2 ?  RecordsScreens(dispatch): <div />}
            {state.app.page === 3 ?  AttendancesScreens(dispatch): <div />}
            {state.app.page === 4 ?  FinanceScreens(dispatch): <div />}
            {state.app.page === 5 ?  ArchiveScreens(dispatch): <div />}
            {state.app.page === 6 ?  SettingsScreens(dispatch): <div />}
            {state.app.page === 7 ?  LogoutScreens(dispatch): <div />}
        </Nav>
    </Navbar>
  );
};

export default Header;
