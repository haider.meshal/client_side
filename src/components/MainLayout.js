import React from 'react';

import Sidebar from 'components/Sidebar';
import Header from 'components/Header';
import Content from 'components/Content';
import Footer from 'components/Footer';

const isSidebarOpen = () => {
  return document
    .querySelector('.cr-sidebar')
    .classList.contains('cr-sidebar--open');
};

const openSidebar = openOrClose => {
  if (openOrClose === 'open') {
    return document
      .querySelector('.cr-sidebar')
      .classList.add('cr-sidebar--open');
  }
  document.querySelector('.cr-sidebar').classList.remove('cr-sidebar--open');
};

const checkBreakpoint = breakpoint => {
  switch (breakpoint) {
    case 'xs':
    case 'sm':
    case 'md':
      return openSidebar('close');

    case 'lg':
    case 'xl':
    default:
      return openSidebar('close');
  }
};

const MainLayout = props => {
  const { children } = props;

  const handleContentClick = e => {
    if (
      isSidebarOpen() &&
      (props.breakpoint === 'xs' ||
        props.breakpoint === 'sm' ||
        props.breakpoint === 'md')
    ) {
      openSidebar('close');
    }
  };
  return (
    <main className="cr-app bg-light">
      <Sidebar />
      <Content fluid onClick={e => handleContentClick(e)}>
        <Header />
        {children}
        <Footer />
      </Content>
    </main>
  );
};
export default MainLayout;
