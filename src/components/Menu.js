import { MdDashboard} from 'react-icons/md';

export const DashboardNavItems = [
  { to: '/dashboard', name: 'dashboard', exact: true, Icon: MdDashboard },
];

export const EmployeesNavItems = [
  { to: '/employeerecords', name: 'records', exact: true, Icon: MdDashboard },
  {
    to: '/employeeapplications',
    name: 'applications',
    exact: true,
    Icon: MdDashboard,
  },
  { to: '/employeeattendances', name: 'attendances', exact: true, Icon: MdDashboard },
  { to: '/employeerewards', name: 'rewards', exact: true, Icon: MdDashboard },
  { to: '/employeededucts', name: 'deducts', exact: true, Icon: MdDashboard },
  { to: '/employeewarnings', name: 'warnings', exact: true, Icon: MdDashboard },
  { to: '/employeeimport', name: 'import', exact: true, Icon: MdDashboard },
];

export const AttendanceNavItems = [
  { to: '/schedules', name: 'schedules', exact: true, Icon: MdDashboard },
  { to: '/timesheets', name: 'timesheets', exact: true, Icon: MdDashboard },
  { to: '/absences', name: 'absences', exact: true, Icon: MdDashboard },
  { to: '/overtimes', name: 'overtimes', exact: true, Icon: MdDashboard },
  { to: '/holidays', name: 'holidays', exact: true, Icon: MdDashboard },
  { to: '/biometric', name: 'biometric', exact: true, Icon: MdDashboard },
  { to: '/import', name: 'import', exact: true, Icon: MdDashboard },
];

export const FinanceNavItems = [
  { to: '/payroll', name: 'payroll', exact: true, Icon: MdDashboard },
  { to: '/rewards', name: 'rewards', exact: true, Icon: MdDashboard },
  { to: '/deducts', name: 'deducts', exact: true, Icon: MdDashboard },
  {
    to: '/compensations',
    name: 'compensations',
    exact: true,
    Icon: MdDashboard,
  },
  { to: '/recognitions', name: 'recognitions', exact: true, Icon: MdDashboard },
  { to: '/accounts', name: 'accounts', exact: true, Icon: MdDashboard },
  { to: '/recievables', name: 'recievables', exact: true, Icon: MdDashboard },
  { to: '/payables', name: 'payables', exact: true, Icon: MdDashboard },
  { to: '/import', name: 'import', exact: true, Icon: MdDashboard },
];
export const SettingsNavItems = [
  { to: '/company', name: 'company', exact: true, Icon: MdDashboard },
  { to: '/heirarchy', name: 'heirarchy', exact: true, Icon: MdDashboard },
  { to: '/security', name: 'security', exact: true, Icon: MdDashboard },
  { to: '/masters', name: 'masters', exact: true, Icon: MdDashboard },
  { to: '/import', name: 'import', exact: true, Icon: MdDashboard },
];

export const ArchiveNaveItems = [
  { to: '/inbound', name: 'inbound', exact: true, Icon: MdDashboard },
  { to: '/outbond', name: 'outbound', exact: true, Icon: MdDashboard },
  { to: '/local', name: 'local', exact: true, Icon: MdDashboard },
];

export const AttendanceReportsNaveItems = [
  { to: '/report_attendance', name: 'attendances', exact: true, Icon: MdDashboard },
  { to: '/report_absent', name: 'absents', exact: true, Icon: MdDashboard },
  { to: '/report_execuse', name: 'execuses', exact: true, Icon: MdDashboard },
  { to: '/report_permission', name: 'permissions', exact: true, Icon: MdDashboard },
  { to: '/report_mission', name: 'missions', exact: true, Icon: MdDashboard },
  { to: '/report_employeeonduty', name: 'on duty', exact: true, Icon: MdDashboard },
  { to: '/report_employeeactivity', name: 'activities', exact: true, Icon: MdDashboard }
];
