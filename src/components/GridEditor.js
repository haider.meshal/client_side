import React from 'react';
import { Alert, Button, Row, Col } from 'reactstrap';

const GridEditor = props => {
  const [localState, setLocalState] = React.useState({});

  // Init LocalState of Data...
  const cloneState = () => {
    let clone = { ...props.data };
    Object.keys(clone).map(key => {
      clone[key] = { ...clone[key], TODO: '' };
    });
    setLocalState(clone);
  };

  return (
    <div>
      {'LOCAL STATE: ' + JSON.stringify(localState)}
      <br />
      {'GLOBAL STATE: ' + JSON.stringify(props.data)}
      <Button
        onClick={() => {
          cloneState();
        }}
      >
        ALTER
      </Button>
    </div>
  );
};
export default GridEditor;

/*
const localStateInit = {
  deleted: {},
  updated: {},
  added: {},
  view: true,
};

const propsDefinition = {
  code: {
    order: 1,
    input: 'text',
    title: 'CODE',
    editable: true,
    validate: '',
  },
  title: {
    order: 2,
    input: 'text',
    title: 'TITLE',
    editable: true,
    validate: '',
  },
};

const Headers = dataDefinitions => {
  return (
    <Row>
      {Object.keys(dataDefinitions)
        .sort((a, b) => {
          return dataDefinitions[a].order - dataDefinitions[b].order;
        })
        .map((header, index) => {
          return <Col key={index}> {dataDefinitions[header].title}</Col>;
        })}
    </Row>
  );
};

const InputType = (key,definition,value) => {
   if (definition[key].input === 'text') return <input name={key} value={value} />
    else return <div> {value} </div>
}

const RenderRows = (data, dataDefinitions) => {
  //1. Sorted List of records...i.e. ROWS
  const sortedRows = Object.keys(data).sort((a, b) => {
    return data[a].code - data[b].code;
  });
  return sortedRows.map(index => {
    return (
      <Row key={index}>
        {//2. Sort keys based on definition..i.e. COL
        Object.keys(data[index])
          .sort((a, b) => {
            return dataDefinitions[a].order - dataDefinitions[b].order;
          })
          .map((key, index2) => {
            console.log(key)
            return <Col key={index2}>{InputType(key,dataDefinitions, data[index][key])}</Col>;
          })}
      </Row>
    );
  });

  //console.log(JSON.stringify(data));
  {
    return Object.keys(dataDefinitions)
      .sort((a, b) => {
        return dataDefinitions[a].order - dataDefinitions[b].order;
      })
      .map((key) => {
        Object.keys(data).map(
          (field, index)=> {
             return <Col key={index}>data[field].title</Col>;
          }
        );
      });
  }
  return (
    <Row>
      {Object.keys(data)
        .sort((a, b) => {
          return data[a].code - dataDefinitions[b].code;
        })
        .map((key, index) => {
          console.log(JSON.stringify(data[key]));
          //Object.keys(data[key]).map((field,index)=>{
          //  console.log(field);
          //  let input = <div> </div>;
          //  return <Col key={index}>{input}</Col>;
          //})
          //if (dataDefinitions[key].input === 'text') input = <input name={key} value={data[key]} />;
        })}
    </Row>
  );
};

const NewRow = dataDefinitions => {
  return (
    <Row>
      {Object.keys(dataDefinitions)
        .sort((a, b) => {
          return dataDefinitions[a].order - dataDefinitions[b].order;
        })
        .map((header, index) => {
          let input = <div> </div>;
          if (dataDefinitions[header].input === 'text')
            input = <input name={header} />;
          return <Col key={index}>{input}</Col>;
        })}
    </Row>
  );
};

const propsPostFunction = () => {
  console.log('called post');
};

const LookupEditor = () => {
  const [localState, setLocalState] = React.useState(localStateInit);
  const nextAddedRecord = Object.keys(localState.added).length + '';
  return (
    <Row>
      {' '}
      <Col>
        <Alert>
          <div> HEADER</div>
          <div>
            {localState.view ? (
              <Button
                onClick={() => setLocalState({ ...localState, view: false })}
              >
                ALTER
              </Button>
            ) : (
              <Button
                onClick={() => {
                  setLocalState({
                    ...localState,
                    added: {
                      ...localState.added,
                      [nextAddedRecord]: {
                        code: 'NEW CODE',
                        title: 'NEW TITLE',
                      },
                    },
                  });
                }}
              >
                ADD
              </Button>
            )}
            {Headers(propsDefinition)}
            {RenderRows(localState.added, propsDefinition)}
          </div>
          {!localState.view ? (
            <div>
              <Button
                onClick={() => {
                  propsPostFunction();
                  setLocalState({ ...localState, view: true });
                }}
              >
                SAVE
              </Button>
              <Button
                onClick={() => {
                  setLocalState({
                    ...localState,
                    added: {},
                    deleted: {},
                    updated: {},
                    view: true,
                  });
                }}
              >
                CANCEL
              </Button>
            </div>
          ) : (
            <div> </div>
          )}
        </Alert>
      </Col>
      <Col>
        <Alert>
          <pre>{JSON.stringify(localState, null, 4)}</pre>
        </Alert>
      </Col>
    </Row>
  );
};

export default LookupEditor;

  */
