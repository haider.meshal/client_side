import React from 'react';
import { Store } from 'state/Store';
import { SWITCH_PAGE } from 'state/reducers';

import sidebarBgImage from 'assets/images/sidebar-background.jpg';

import { Nav, Navbar, NavItem, NavLink as BSNavLink } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { MdDashboard, MdKeyboardArrowDown } from 'react-icons/md';

import Logo from 'components/Logo';
import SourceLink from 'components/SourceLink';

import bn from 'utils/bemnames';

const bem = bn.create('sidebar');

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const Sidebar = () => {
  const { state, dispatch } = React.useContext(Store);
  return (
    <aside className={bem.b()} data-image={sidebarBgImage}>
      <div className={bem.e('background')} style={sidebarBackground} />
      <div className={bem.e('content')}>
        <Navbar>
          <SourceLink className="navbar-brand d-flex">
            <Logo />
            <span className="text-white h-100">IDEAL Software</span>
          </SourceLink>
        </Navbar>
        <Nav vertical>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 1  })}
              tag={NavLink}
              to={'/dashboard'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">DASHBOARD</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 2  })}
              tag={NavLink}
              to={'/records'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">RECORDS</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 3  })}
              tag={NavLink}
              to={'/attendances'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">ATTENDANCES</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 4  })}
              tag={NavLink}
              to={'/finance'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">FINANCE</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 5  })}
              tag={NavLink}
              to={'/archive'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">ARCHIVE</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 6  })}
              tag={NavLink}
              to={'/settings'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">SETTINGS</span>
            </BSNavLink>
          </NavItem>
          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              onClick = {()=>dispatch({type: SWITCH_PAGE, payload: 7  })}
              tag={NavLink}
              to={'/logout'}
              className="text-uppercase"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">LOGOUT</span>
            </BSNavLink>
          </NavItem>
        </Nav>
      </div>
    </aside>
  );
};
export default Sidebar;
