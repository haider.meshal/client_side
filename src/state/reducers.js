export const INIT = 'INIT';
export const NORMAL = 'NORMAL';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILED = 'FAILED';
export const SIGNINED_ON = 'SIGNINED_ON';
export const SIGNINED_OFF = 'SIGNINED_OFF';
export const DEFAULT_SCREEN = 'DEFAULT_SCREEN';
export const SWITCH_PAGE = 'SWITCH_PAGE';
export const SWITCH_SCREEN = 'SWITCH_SCREEN';
export const SWITCH_LANGUAGE = 'SWITCH_LANGUAGE';
export const RECIEVED_RECORDS = 'RECIEVED_RECORDS';
export const RECIEVED_RECORD = 'RECIEVED_RECORD';
export const RECIEVED_RECORDS_DOCS = 'RECIEVED_RECORDS_DOCS';
export const RECIEVED_RECORD_DOC = 'RECIEVED_RECORD_DOC';

const rootReducer = (state, action) => {
  switch (action.type) {
    case INIT:
      return { ...state, translation: action.payload.translation };
    case SWITCH_LANGUAGE:
      return { ...state, app: { ...state.app, language: action.payload } };
    case NORMAL:
      return {
        ...state,
        app: { ...state.app, loading: false, success: false, error: false },
      };
    case LOADING:
      return {
        ...state,
        app: {
          ...state.app,
          loading: true,
          success: false,
          error: false,
          message: action.payload,
        },
      };
    case SUCCESS:
      return {
        ...state,
        app: {
          ...state.app,
          loading: false,
          success: true,
          error: false,
          message: action.payload,
        },
      };
    case FAILED:
      return {
        ...state,
        app: {
          ...state.app,
          loading: false,
          success: false,
          error: true,
          message: action.payload,
        },
      };
    case SWITCH_PAGE:
      return { ...state, app: { ...state.app, page: action.payload , screen: 0} };
    case DEFAULT_SCREEN:
      return { ...state, app: { ...state.app, screen: 0 } };
    case SWITCH_SCREEN:
      return { ...state, app: { ...state.app, screen: action.payload } };
    case SIGNINED_ON:
      return {
        ...state,
        app: {
          ...state.app,
          signed: true,
          signed_time: new Date(),
          name: action.payload.name,
          profile_image: action.payload.profile_image,
          token: action.payload.token,
        },
      };
    case SIGNINED_OFF:
      return {
        ...state,
        app: { ...state.app, signed: false, signed_time: '', token: '' },
      };
    default:
      return state;
  }
};

export { rootReducer };
