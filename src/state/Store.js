import React from 'react';
import { rootReducer } from 'state/reducers';

export const Store = React.createContext();

const initialState = {
  app: {
    profile_image: '',
    language: 'en',
    token: '',
    name: '',
    signed: false,
    signed_time: '',
    loading: false,
    success: false,
    error: false,
    message: '',
    page: 0,
    screen: 0,
  },
  translation: {
    en: {
      app_name: 'en arabic name',
    },
    ar: {
      app_name: 'ar arabic name',
    },
  },
  company: {},
  records: {},
  record_docs: {},
  attendances: {},
  hierarchies: {},
  nodes: {},
  settings: {},
};

export const StoreProvider = props => {
  const [state, dispatch] = React.useReducer(rootReducer, initialState);
  const value = { state, dispatch };
  return <Store.Provider value={value}>{props.children}</Store.Provider>;
};
